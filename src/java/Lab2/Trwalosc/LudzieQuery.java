/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab2.Trwalosc;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;


/**
 *
 * @author student
 */
public class LudzieQuery {
    
    private Session session = null;
    private List<Pracownicy> ludzieList = null;
    private Query q = null;
    
    public String getLudzieLista(boolean OrderByImie) {
        try {
            org.hibernate.Transaction tx = session.beginTransaction();
            if (OrderByImie) {
                q = session.createQuery("from Pracownicy order by Imie");
            } else {
                q = session.createQuery("from Pracownicy");
            }
            ludzieList = (List<Pracownicy>) q.list();
            session.close();
            tx.commit();
        } catch (HibernateException e) {
        }
        String data = getListaHTML(ludzieList);
        return data;
    }
    
    private String getListaHTML(List<Pracownicy> lista) {
        String wiersz;
        wiersz = ("<table><tr>");
        wiersz = wiersz.concat(
            "<td><b>ID</b></td>"
            + "<td><b>IMIE</b></td>"
            + "<td><b>NAZWISKO</b></td>");
        wiersz = wiersz.concat("</tr>");
        for (Pracownicy ldz : lista) {
            wiersz = wiersz.concat("<tr>");
            wiersz = wiersz.concat("<tr>" + ldz.getId() + "</td>");
            wiersz = wiersz.concat("<tr>" + ldz.getImie() + "</td>");
            wiersz = wiersz.concat("<tr>" + ldz.getNazwisko() + "</td>");
            wiersz = wiersz.concat("</tr>");
        }
        wiersz = wiersz.concat("</table>");
        return wiersz;
    }
    
    public void Dodanie(String id, String im, String nazw) {
        try {
            org.hibernate.Transaction tx = session.beginTransaction();
            q = session.createQuery("insert into Pracownicy(" + id + ", " + im + ", " + nazw + ")");
            q.executeUpdate();
            session.close();
            tx.commit();
        }
        catch (HibernateException e)
        {}
    }
    
    public LudzieQuery() {
        this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
    }
}
