/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab2.Serwlety;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author student
 */
@WebServlet(name = "Wypisywanie", urlPatterns = {"/Wypisywanie"})
public class Wypisywanie extends HttpServlet {
    
    static String ogr;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        String inst = request.getParameter("instytucja");
        boolean imie = request.getParameter("jakie") != null;
        ogr = request.getParameter("ograniczenie");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head><meta><link rel='stylesheet' href='Style/css/components.css'>");
            out.println("<link rel='stylesheet' href='Style/css/icons.css'>");
            out.println("<link rel='stylesheet' href='Style/css/responsee.css'>");
            out.println("<body>"+"Lista pracownikow instytucji :" + inst + "<br />");
            out.println(new Lab2.Trwalosc.LudzieQuery().getLudzieLista(imie));
            //new Lab2.Trwalosc.LudzieQuery().getLudzieLista(imie);
            //out.println(getDataFromDb());
            out.println("</body>");
            out.println("<a class=\'button rounded-btn s-1 margin-bottom\' href=");
            out.println(request.getHeader("referer"));
            out.println("><i class='icon-sli-arrow-left'>Powrót</i></a>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } 
        catch (SQLException ex) {
            Logger.getLogger(Wypisywanie.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } 
        catch (SQLException ex) {
            Logger.getLogger(Wypisywanie.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean poloczenie = false;
    private String data;
    
    private String getDataFromDb()
    {
        try{
            poloczenie = Lab2.Serwlety.DbManager.Connect();
            if (poloczenie) {
                data = Lab2.Serwlety.DbManager.getData();
                poloczenie = Lab2.Serwlety.DbManager.Disconnect();
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Wypisywanie.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }
    
}
