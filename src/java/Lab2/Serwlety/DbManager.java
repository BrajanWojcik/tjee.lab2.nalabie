/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab2.Serwlety;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author student
 */
public final class DbManager {
    
 public static final String DRIVER = "org.apache.derby.jdbc.ClientDriver";
 public static final String JDBC_URL = "jdbc:derby://localhost:1527/db";
 public static final String QUERY = "select * from PRACOWNICY";
 private static java.sql.Connection conn;

 private DbManager() { }
 
 public static boolean Connect() throws ClassNotFoundException, SQLException {
 conn = DriverManager.getConnection(JDBC_URL);
 if (conn == null) {
 return false;
 } else {
 return true;
 }}
 
 public static boolean Disconnect() throws SQLException {
 if (conn == null) {
 return false;
 } else {
 conn.close();
 return true;
 }}
 
 public static String getData() throws SQLException 
 {
 Statement stat = conn.createStatement();
 ResultSet rs = stat.executeQuery(QUERY);
 ResultSetMetaData rsmd = rs.getMetaData();
 String ograniczen;
 ograniczen = Wypisywanie.ogr;
 String wiersz = new String();
 int colCount = rsmd.getColumnCount();
 int k = 0, e = 0;
 switch (ograniczen) {
     case "d":
         k=1;
         colCount=1;
         break;
     case "di":
         k=1;
         colCount=2;
         break;
     case "dn":
         e=1;
         break;
     case "i":
         k=2;
         colCount=2;
         break;
     case "n":
         k=3;
         colCount=3;
         break;
     case "in":
         k=2;
         colCount=3;
         break;
     case "":
         k=1;
         colCount=3;
         break;
     case "din":
         k=1;
         colCount=3;
         break;
 }
 if (e == 0)
 {
     wiersz = wiersz.concat("<table><tr>");
     for (int i=k; i<=colCount; i++)
     {
         wiersz = wiersz.concat(" <td><b> "+rsmd.getColumnName(i) + "</b></td>");
     }
     wiersz = wiersz.concat("</tr>");
     while (rs.next())
     {
         wiersz = wiersz.concat("<tr>");
         for (int i=k; i<=colCount; i++)
         {
             wiersz = wiersz.concat(" <td> " + rs.getString(i) + " </td> ");
         }
     wiersz = wiersz.concat("</tr>");
     }
     wiersz = wiersz.concat("</table>");
     if (stat != null) {stat.close(); } return wiersz;
 }
 else
 {
     wiersz = wiersz.concat("<table><tr>");
     wiersz = wiersz.concat(" <td><b> "+rsmd.getColumnName(1) + "</b></td>");
     wiersz = wiersz.concat(" <td><b> "+rsmd.getColumnName(3) + "</b></td>");
     wiersz = wiersz.concat("</tr>");
     while (rs.next())
     {
         wiersz = wiersz.concat("<tr>");
         wiersz = wiersz.concat(" <td> " + rs.getString(1) + " </td> ");
         wiersz = wiersz.concat(" <td> " + rs.getString(3) + " </td> ");
         wiersz = wiersz.concat("</tr>");
     }
     wiersz = wiersz.concat("</table>");
     if (stat != null) {stat.close(); } return wiersz;
 }
 }
}
